import matplotlib.pyplot as plt
import numpy as np

tabx = []
taby = []


def siln(n):
    silnia = 1
    for i in range(2, n + 1):
        silnia *= i
    return silnia


def suma_ulamkow(n):
    suma = 0
    for x in range(n):
        suma += 1 / siln(x)
    return suma


for i in range(10):
    tabx.append(i)
    taby.append(
        suma_ulamkow(i)
    )

# Define limits of coordinate system
x1 = -8
x2 = 11
y1 = -8
y2 = 8

plt.xlim(left=x1)
plt.xlim(right=x2)
plt.ylim(bottom=y1)
plt.ylim(top=y2)
plt.axhline(linewidth=2, color='k')
plt.axvline(linewidth=2, color='k')

##plt.grid(True)
plt.grid(color='k', linestyle='-.', linewidth=0.5)

plt.plot(tabx, taby, 'o')
plt.show()
