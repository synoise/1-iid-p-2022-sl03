import random
import numpy as np

ilosc_losowan = 3
l_losowych = 5
losy = []
wygrane = []
liczba_hazardzistow = 100000

liczba_wygranych = 0
liczba_sprawdzonych = 0


def losowa_tab(n):
    tab = []
    for x in range(n):
        tab.append(random.randint(1, 8))
    return tab


for i in range(liczba_hazardzistow):
    losy.append(losowa_tab(l_losowych))

losy_np = np.array(losy)

for z in range(ilosc_losowan):
    wygrane.append(losowa_tab(l_losowych))

wygrane_np = np.array(wygrane)

for nr_losu in range(liczba_hazardzistow):
    for i in range(ilosc_losowan):
        liczba_sprawdzonych += 1
#       if np.array_equal(losy[nr_losu], wygrane[i]):
        ilosc_poprawnych = list(losy_np[nr_losu] == wygrane_np[0]).count(True)
        if ilosc_poprawnych > 2:
            print(losy[nr_losu], nr_losu, i, ilosc_poprawnych)
            liczba_wygranych += 1

print(f"Liczba wygranych = {liczba_wygranych}")
print(f"Liczba sprawdzonych losów = {liczba_sprawdzonych}")
print(f"Wynik losowania: {wygrane_np}")