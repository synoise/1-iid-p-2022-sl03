import random

slownik = [{"znak": "A", "x": 2}, {"znak": "B", "x": 2}, {"znak": "C", "x": 3}]


def losowanie():
    rolka = slownik
    return rolka[random.randint(0, len(rolka)-1)]


monety = int(input("Podaj ilosc monet: "))
stawka = 2

while monety > 0:
    nowaWartosc = input("Wprowadz stawke: ")

    if nowaWartosc == "":
        monety -= int(stawka)
    else:
        stawka = nowaWartosc
        monety -= int(stawka)

    print("=====", "\n", losowanie()['znak'], losowanie()['znak'], losowanie()['znak'], "\n Ilosc monet: ")