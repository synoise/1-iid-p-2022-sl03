import random
#Wiedząc, że 1233 = 122 + 332, napisz program, który znajduje wszystkie liczby z przedziału od 1000
#do 9999 spełniające taką ciekawą zależność. Program dodatkowo liczy, ile ich jest.

# a = 0
# for i in range(10,100):
#     for j in range(10,100):
#         wynik = i**2 + j**2
#         liczba = str(i)+str(j)
#         if str(wynik) == str(liczba):
#             print(i,"+",j,"=",wynik)
#             a += 1
#         if i == 99 and j == 99:
#             print("Znalazłem",a,"przypadków, kiedy zależność sie spełnia" )

import matplotlib.pyplot as plt
import numpy as np

# Define limits of coordinate system
x1 = -8
x2 = 8
y1 = -8
y2 = 8

plt.xlim(left=x1)
plt.xlim(right=x2)
plt.ylim(bottom=y1)
plt.ylim(top=y2)
plt.axhline(linewidth=2, color='k')
plt.axvline(linewidth=2, color='k')

##plt.grid(True)
plt.grid(color='k', linestyle='-.', linewidth=0.5)

tabx = [1,5,7,3,1,2,2]
taby = [2,7,3,1,6,8,3]
plt.plot(tabx, taby, 'o')
plt.show()





