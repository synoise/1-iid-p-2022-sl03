''' 9.
Napisz program w Pythonie, aby uzyskać trzy najlepsze produkty w sklepie.
Przykładowe dane: {'item1': 45.50, 'item2':35, 'item3': 41,30, 'item4':55, 'item5': 24}
Oczekiwany wynik: item4 : 55, item1:45.5 , item3 41.3
9.
Напишите программу на Python, чтобы получить три лучших товара в магазине.
Пример данных: {'item1': 45,50, 'item2': 35, 'item3': 41,30, 'item4': 55, 'item5': 24}
Ожидаемый результат: элемент 4: 55, элемент 1: 45,5, элемент 3 41,3. '''
# import operator
#
# a = {'item1': 45.50, 'item2':35, 'item3': 41.30, 'item4':55,'item5': 24}
#
# def wynik():
#     # for i in a.items():
#     x = max(a.items(), key=operator.itemgetter(1))
#
#     print(x)  # напечатать пару ('C', 3)
#     print(x[0])
# wynik()

a = {'item1': 45.50, 'item2':35, 'item3': 41.30, 'item4':55,'item5': 24}
lepsz = {}
pomoc = []

for x in a.values():
    pomoc.append(x)

for i in range(3):
    print(list(a.keys())[list(a.values()).index(max(pomoc))],':',max(pomoc),end = ', ')

    a.pop(list(a.keys())[list(a.values()).index(max(pomoc))])
    pomoc.pop(pomoc.index(max(pomoc)))


''' 10.	
Napisz program w Pythonie, który konwertuje listę na zagnieżdżony słownik kluczy. Przykład: {1: {2: {3: {4: {}}}}}
10.
Напишите программу на Python, которая преобразует список во вложенный словарь ключей. Пример: {1: {2: {3: {4: {}}}}} '''