''' Wiedząc, że 1233 = 122 + 332, napisz program,
który znajduje wszystkie liczby z przedziału od 1000
do 9999 spełniające taką ciekawą zależność. Program
dodatkowo liczy, ile ich jest.
Зная, что 1233 = 122 + 332, напишите программу, которая найдет все числа в диапазоне от 1000
до 9999 года удовлетворял такие интересные отношения. Программа дополнительно подсчитывает, сколько их.'''

# illosc = 0
#
# for i in range(1000,10000):
#     pdc = i // 100
#     odc = i % 100
#     if pdc**2 + odc**2 == i:
#         print(i, pdc, odc)
#         illosc +=1
#
# print(illosc)

''' Liczbę można obliczać na wiele sposobów, np. metodą Monte Carlo. 
Napisz program, który oblicza
liczbę z określoną dokładnością, korzystając z rysunku 3.10 i 
następującej listy kroków:
1. Wpisz koło o promieniu r w kwadrat o boku 2 ∙ r.
2. Losowo wygeneruj punkty i umieść je w kwadracie.
3. Wyznacz liczbę punktów, które znajdują się jednocześnie w 
kwadracie i w kole.
4. Niech promień r będzie wyznaczony przez stosunek liczby 
punktów znajdujących się w kole
do liczby punktów znajdujących się w kwadracie.
5. π~4.0∙r 
Число может быть рассчитано многими способами, например, методом Монте-Карло. Напишите программу, которая вычисляет
число с заданной точностью, используя рисунок 3.10 и следующий список шагов:
1. Введите окружность с радиусом r в квадрат со стороной 2 ∙ r.
2. Случайным образом генерируйте очки и размещайте их в квадрате.
3. Определите количество точек, которые одновременно находятся в 
в квадрате и по кругу.
4. Пусть радиус r определяется отношением числа 
точки, расположенные в круге
к количеству точек в квадрате.
5. π ~ 4.0∙r '''