''' 1.	Dodaj nową pozycję do listy po określonej pozycji:
list1 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
zmień na [10, 20, [300, 400, [5000, 6000, 7000], 500], 30, 40] '''

# b = 7000
# a = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
# a[2][2].append(b)
# print(a)

''' 2.	Podnieś każdy element listy zawierającej liczby, do potęgi ustalonej przez użytkownika.
Поднимите каждый элемент списка, содержащего числа, до степени, установленной пользователем '''

# a = [2, 'sdjk', [], 4, 'df', 76, 3]
#
# def dfjfds():
#     b = []
#     n = int(input('dodaj poteng: '))
#     for i in a:
#         if type(i) == type(5):
#             i**=n
#         b.append(i)
#
#     return(b)
#
# x = dfjfds()
#
# print(x)

''' 3.	Napisz program, który doda dwie listy według indeksu, tak żeby 
lista1 i lista 2 pokazały logiczne zdanie:
list1 = ["M", "n", "im", "Pio"]
list2 = ["am", "a", "ie", "tr"]
 '''

# list1 = ["M", "n", "im", "Pio"]
# list2 = ["am", "a", "ie", "tr"]
#
# def edenPluSdwa():
#     b = ''
#     for i in range(len(list1)):
#         b += list1[i]+list2[i]+' '
#     return b
#
# x = edenPluSdwa()
# print(x)

''' 4.	Przekształć dwie listy w słownik.
 4.	Преобразуйте два списка в словарь. '''


# list1 = ["M", "n", "im", "Pio"]
# list2 = ["am", "a", "ie", "tr"]
#
# def edenPluSdwa():
#     dict1 = {}
#     for key in list1:
#         for value in list2:
#             dict1[key] = value
#             list2.remove(value)       #РАЗОБРАТЬ   https://translated.turbopages.org/proxy_u/en-ru.ru.35ea2806-6391c266-4bd32350-74722d776562/https/www.studytonight.com/python-howtos/convert-two-lists-to-dictionary-in-python
#             break
#     return dict1
#
# x = edenPluSdwa()
# print(x)


# list1 = ["M", "n", "im", "Pio"]
# list2 = ["am", "a", "ie", "tr"]
# def edenPluSdwa():
#     c = dict(zip(list1, list2))    #РАЗОБРАТЬ  https://ru.stackoverflow.com/questions/789625/%D0%9A%D0%B0%D0%BA-%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C-%D1%81%D0%BB%D0%BE%D0%B2%D0%B0%D1%80%D1%8C-%D0%B8%D0%B7-%D0%B4%D0%B2%D1%83%D1%85-%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D0%BE%D0%B2?ysclid=lbeyl7qzxa481869804
#     return c
#
# x = edenPluSdwa()
# print(x)

''' 5.	Połącz dwa słowniki Pythona w jeden.
 5.	Объедините два словаря Python в один.'''

# https://egorovegor.ru/python-merge-dict/?ysclid=lbfnajobv311171290

# wokabl1 = {'1':'1','dff':'3'}
# wokabl2 = {'2':'2','sxs':'32'}

# def newWokabls():
#     newWokabl = {**wokabl1,**wokabl2}    # записать вариант работает в python до 3.9
#     # newWokabl = dict(list(wokabl1.items()) + list(wokabl2.items()))      # записать вариант работает в python до 3.9
#     # newWokabl = wokabl1 | wokabl2        # записать вариант работает от python 3.9
#
    # return newWokabl

# wokabl3 = newWokabls()
# print(wokabl3)

''' 6.	Wyświetl na konsoli wartość klucza „history” z poniższego:
slownik = { "class": { "student": { "name": "Mike", "marks": { "physics": 70, "history": 80 } } } }
 '''

# https://pythobyte.com/nested-dictionary-python-67006/?ysclid=lbfoemboz1417510467  #не сильно,но можно посмотреть создание вложенных словарей
#https://pykili.github.io/prog/dict-2
#https://proglib.io/p/15-veshchey-kotorye-nuzhno-znat-o-slovaryah-python-2020-03-03?ysclid=lbfoond79j134769721            #под вопросом

# dictionary = { "class": { "student": { "name": "Mike", "marks": { "physics": 70, "history": 80 } } } }
# print(dictionary['class']['student']["marks"]['history'])        #https://tonais.ru/dictionary/vlozhennye-slovari-python?ysclid=lbfoegx5yn756240621

''' пометки '''
# dict_of_lists = {'Ксюша': ['ручка', 'ручка', 'карандаш', 'ластик',],'Миша': ['ручка', 'мелок', 'линейка',]}
# print(dict_of_lists['Миша'][1])                                        # ВЫПИСАТЬ  как достать эл-т из вложенного списка в словарь

''' пометки '''
# dict_of_lists = {'Ксюша':{'ручка': 1, 'карандаш':0, 'ластик':3} }      #ВЫПИСАТЬ   как добавить эл-т во вложенный словарь
# dict_of_lists['Ксюша']['ластик'] += 1
# print(dict_of_lists)

''' 7.	Napisz funkcję, która przyjmuje wartość temperatury w Kelvinach i zwraca wartość wyrażoną 
w stopniach Celsjusza: TC = TK − 272.15.W przypadku podania wartości ujemnej funkcja zwraca None. 
Przetestuj jej działanie.
 Напишите функцию, которая принимает значение температуры в кельвинах и возвращает значение, выраженное 
в градусах Цельсия: TC = TK − 272.15.Если задано отрицательное значение, функция возвращает None. 
Проверьте его работу.'''

# tempKelw = float(input('print temp in Kelwin: '))
#
# def tempKelwnWcels(TK):
#     if TK >= 0:
#         TC = TK - 272.15
#         return TC
#     else:
#         return None
#
# tempCel = tempKelwnWcels(tempKelw)
#
# print(tempCel)

''' 8.	Stwórz funkcję, która rysuje choinkę. Program powinien zawierać funkcję, przyjmującą parametr, 
określający wielkość choinki.
Создайте функцию, которая рисует елку. Программа должна содержать функцию, принимающую параметр, 
определение размера елки. '''

# https://know-online.com/post/python-crop-textpovtorenie-stroki

''' пример с лабы '''

# def choinka(wielkość):
#     txt = "*"
#
#     space = "1"
#     for i in range(wielkość):
#         space = space + "22"
#     for i in range(wielkość):
#         print(space[i:] + txt)
#         if i < (wielkość - 2):
#             txt = txt + "**"
#         # else:
#         #     txt = ""
#         #     for j in range(wielkość):
#         #         txt = txt + "++"
#
#
# choinka(5)

''' мое решение'''

# elk = int(input('write size  spruce: '))
#
# def spruce(elk):
#     g,s = '',''
#     for i in range(elk):
#         g += '*'
#         print((elk*"_")+s+g)
#         elk -= 1
#         g += '*'
# spruce(elk)

''' 9.	Napisz program  wypisujący 10 liczb w jednym wierszu.
 Напишите программу, которая выводит 10 чисел в одну строку.'''

# ile = int(input('dodaj ilocz liczb: '))
#
# def wiersz(ile):
#     s = ''
#     for i in range(ile):
#         s += input('dodaj liczbu: ') + ' '
#     return s
#
# newWiersz = wiersz(ile)
# print(newWiersz)

''' 10.	Napisz program rysujący macierz wypełnioną kolejnymi,  liczbami naturalnymi od 0. 
Program powinien zawierać funkcję, która przyjmuje parametr określający rozmiar 
macierzy (np. 7x7 lub 12x12 …) 
10.	Напишите программу, которая рисует матрицу, заполненную последовательными натуральными 
числами от 0. Программа должна содержать функцию, которая принимает параметр, определяющий 
размер матрицы (например, 7x7 или 12x12 ...)'''

# def matrica(n):
#     m = n
#     c = []
#     k = 0
#     for i in range(n):
#         a = []
#         for r in range(k,n):
#             a.append(r)
#         c.append(a)
#         k,n = n,n+m
#     print(c)
#
# matrica(int(input("dodaj razmer matricy: ")))



'''________________________________________________________'''

# ile = int(input('dodaj iloszcz: '))
#
# def macierza():
#     list = []
#     list1 = []
#     for k in range(3):
#         for i in range(1,10):
#             list.append(1,10)
#         list1.append(list)
#     print(list1)
# macierza()
#
# def prOg(par):
#     s = []
#     for i in range(par*10, par*10+10):
#         s.append(i)
#     return s
# a = prOg(1)
# print(a)
# def mAt(par):
#     s = []
#     for i in range(par):
#         s.append(prOg(i))
#     return s
# e = mAt(5)
# print(e)

''' 11.	 Zmodyfikuj powyższy program program tak, aby wypisywał tabliczkę mnożenia.
 11.	 Измените приведенную выше программу так, чтобы она печатала таблицу умножения.'''

''' Не РАБОТАЕТ '''

# def matrica(n):
#     m = n
#     c = []
#     k = 0
#
#     for i in range(n):
#         a = []
#
#         for r in range(n):
#             x = 0
#             x = i*r
#             print(x)
#             for j in range(k,n):
#                 s = ""
#                 s += str(j)+"*"+str(i)+"="+str(x)
#             a.append(s)
#         c.append(a)
#         k,n = n,n+m
#     print(c)
#
# matrica(int(input("dodaj razmer matricy: ")))



''' 12.	Napisz program, który posiada funkcję fill(n), wypełniającą tablicę o rozmiarze 
n (podanym przez użytkownika), liczbami losowymi, z zakresu od 1 do 10. Dodatkowo program 
musi posiadać funkcję wypisującą elementy tablicy na ekranie, w jednym wierszu oraz funkcję search(n), 
wyszukującą wystąpienie liczby n, w wypełnionej tablicy. 
12.	Напишите программу, которая имеет функцию fill (n), заполняющую массив размера n (заданный пользователем) 
случайными числами в диапазоне от 1 до 10. Кроме того, программа должна иметь функцию, которая выводит элементы 
массива на экран в одной строке, и функцию search (n), которая ищет вхождение числа n, в заполненном массиве.  '''

import random

n = int(input("rozmiar tablicy: "))

def fill(n):
    c = []
    m = n
    k = 0
    for i in range(n):
        a = []
        for r in range(n):
            a.append(random.randint(1,10))
        c.append(a)
        k,m = m,m+n
    return c
newTabl = fill(n)

# print(newTabl)

def vyvod(tabl):
    s = ''
    for i in tabl:
        for r in i:
            s += str(r)+" "
    return s

stroka = vyvod(newTabl)

# print(stroka)

def search(stroka,n):
    c = 0
    for i in stroka:
        if i == str(n):
            c += 1
    return c

ileN = search(stroka,n)

print(newTabl)
print(stroka)
print("v tablice ",ileN," liczby ""'",n,"'")

'''13 Napisz program,  który w tablicy dwuwymiarowej 10×10 umieszcza losowo na przekątnej liczby od 0 do 9, a 
poza nią zera. Dodatkowo oblicza on sumę liczb znajdujących się na przekątnej. Klasa powinna zawierać trzy metody:
czytajDane() — umieszcza dane w tablicy,
przetworzDane() — oblicza i wyświetla sumę liczb znajdujących się na przekątnej,
wyswietl_wynik() — pokazuje zawartość tablicy na ekranie monitora.
Напишите программу, которая в двумерном массиве 10×10 случайным образом помещает по диагонали число от 0 до 9, а 
за его пределами-нули. Кроме того, он вычисляет сумму чисел, расположенных по диагонали. Класс должен содержать три метода:
readdane () - помещает данные в массив,
processed () - вычисляет и отображает сумму чисел, расположенных по диагонали,
wyslow_результат () - показывает содержимое массива на экране монитора. '''

''' только массив и сумма в одной ф-ии '''

# import random
#
# n = int(input("rozmiar tablicy: "))
#
# def czytajDane(n):
#     c = []
#     m = n
#     k = 0
#     summ = 0
#     for i in range(n):
#         a = []
#         for r in range(n):
#             if r == i:
#                 a.append(random.randint(0,9))
#                 summ += a[r]
#             else:
#                 a.append(0)
#         c.append(a)
#         k,m = m,m+n
#     return c,summ
# newTabl,summa = czytajDane(n)
#
# print("nowa tablica jest: ",newTabl)
# print("summa = ",summa)

''' доделанная до конца '''

# import random
#
# n = int(input("rozmiar tablicy: "))
#
# def czytajDane(n):
#     c = []
#     m = n
#     k = 0
#     for i in range(n):
#         a = []
#         for r in range(n):
#             if r == i:
#                 a.append(random.randint(0,9))
#             else:
#                 a.append(0)
#         c.append(a)
#         k,m = m,m+n
#     return c
# newTabl = czytajDane(n)
#
# def przetworzDane(tabl):
#     summ = 0
#     for i in tabl:
#         for j in i:
#             if j == 0:
#                 pass
#             else:
#                 summ += j
#     print("summa jest: ",summ)
#
# przetworzDane(newTabl)
#
# def wyswietl_wynik(tabl):
#     print(tabl)
# wyswietl_wynik(newTabl)


''' 14.	Napisz program,  który w tablicy dwuwymiarowej 10×10 umieszcza losowo na jednej części po przekątnej liczby 
od 0 do 9, wraz z przekątną, a poza nią zera.  Napisz funkcję, która odwraca tę macierz symetrycznie w osi przekątnej 
powstałej macierzy. 
14.	Напишите программу, которая в двумерном массиве 10×10 случайным образом помещает на одну диагональную часть числа 
от 0 до 9 вместе с диагональю, а за ее пределами-нули.  Напишите функцию, которая переворачивает эту матрицу симметрично 
по диагональной оси полученной матрицы.'''

# import random
#
# n = int(input("rozmiar tablicy: "))
#
# def czytajDane(n):
#     c = []
#     m = n
#     k = 0
#     for i in range(n):
#         a = []
#         for r in range(n):
#             if r <= i:
#                 a.append(random.randint(0,9))
#             else:
#                 a.append(0)
#         c.append(a)
#         k,m = m,m+n
#     return c
# newTabl = czytajDane(n)
#
# print("nowa tablica jest: ",newTabl)
#
# def reversTabl(tab):
#     a = []
#     for i in tab:
#         i.reverse()           #https://pythonim.ru/list/obratnyy-spisok-v-python?ysclid=lbtrpvjger956082313
#         a.append(i)
#     return a
# reversTabl = reversTabl(newTabl)
#
# print("revers tablica jest: ",reversTabl)

''' na 3 '''

'''Funkcja wypełniająca tablicę losowymi liczbami
Stwórz funkcję, która wypełni tablicę losowymi liczbami i wpisze je do listy (tablicy). 
Wielkość listy powinna być przekazana jako parametr funkcji.
Następnie stwórz funkcję, która znajdzie wartość minimalną i maksymalną w tablicy.
Создайте функцию, которая заполнит массив случайными числами и введет их в список (массив). 
Размер списка должен быть передан в качестве параметра функции.
Затем создайте функцию, которая найдет минимальное и максимальное значения в массиве'''

# import random
#
# def tabLica(a):
#     b = []
#     for i in range(a):
#         b.append(random.randint(1,100))
#     return b
#
# def paramTablicy(a):
#     print('wartosc maximalna: ',max(a),',','wartosc minimnalna: ',min(a))
#
# iloszczTablicy = int(input("dodaj iloszcz tabliczy: "))
#
# newTablica = tabLica(iloszczTablicy)
#
# paramTablicy(newTablica)

''' na 5 '''

''' Napisz program , który zawiera listę o nazwie names z róznymi imionami oraz listę o 
nazwie surnames z różnymi nazwiskami .Następnie stwórz funkcję która wypelni listę słownikami , 
zawierającymi pola name , surname , age , phone . Pola te powinny być kolejno , wypełnione : 
e powinny losowymi imionami pobranymi z tablicy names , losowymi nazwiskami pobranymi z tablicy 
sumames , Noadwym losowymi liczbami z przedziału od 18 do 70 , reprezentujące wiek oraz losowymi 
liczbami z przedzialu od 5000000 do 8000000 , reprezentujące numer telefonu . Wielkość listy 
( tablicy ) zawierającej słowniki powinna być określona przez parametr przekazany do funkcji . 
Następnie stwórz funkcję która wyszuka w wygenerowanej liście , dowolny z parametrów i zwróci 
pasujące rekordy .

Напишите программу, в которой есть список с именем names с разными именами и список с именем surnames 
с разными именами . Затем создайте функцию, которая заполнит список словарями, содержащими поля 
name , surname , age, phone . Эти поля должны быть последовательно заполнены: случайными именами , 
взятыми из массива names , случайными именами , взятыми из массива sumames , Noad-случайными числами 
от 18 до 70, представляющими возраст, и случайными числами от 5000000 до 8000000, представляющими 
номер телефона . Размер списка (массива), содержащего словари, должен определяться параметром, 
переданным функции . Затем создайте функцию, которая будет искать в сгенерированном списке любой из 
параметров и возвращать соответствующие записи . '''

# https://qna.habr.com/q/903677?ysclid=lbe8a4wpjv456103291
# https://egorovegor.ru/python-select-random-element/?ysclid=lbe6x9xurn915446492
# https://www.delftstack.com/ru/howto/python/python-search-list-of-dictionaries/


# import random
#
# surnames = ['Ushkevich', 'Voronko', 'Kurczak', 'Slaha']
# names = ['Siarhei','Valiantsin','Gleb','Anton']
# wielkosc = int(input('dodaj wielkosc listy: '))
#
# def dodajVtablicu(length):
#     a = []
#     for i in range(length):
#         a.append(dict( name = random.choice(names),
#                        surname = random.choice(surnames),
#                        age = random.randint(18,70),
#                        numar = random.randint(5000000,8000000)))
#         # for i in surnames:
#         #     a.append(dic)
#     return a
#
# newSlownik = dodajVtablicu(wielkosc)
# print(newSlownik)
#
# def wyszuka():
#     lis = []
#     for i in range(len(newSlownik)):
#         c = input('"shukam" lub "nie shukam": ')
#         if c == "shukam":
#             a = input('co szuka? "name","surname","age","numar": ')
#     # if a == 'name':
#     #     for i in newSlownik:
#     #         lis.append(i.get('name', None))       #разобрать ситуацию i.get
#     # if a == 'surname':
#     #     for i in newSlownik:
#     #         lis.append(i.get('surname', None))
#     # if a == 'age':
#     #     for i in newSlownik:
#     #         lis.append(i.get('age', None))
#             for i in newSlownik:
#                 lis.append(i.get(a, None))
#     print(lis)
# wyszuka()


''' ВЫКЛАДЫ !!!!!!!!!!!!! '''

# import pandas as pd
# a = [1,7,2]
# myvar = pd.Series(a)
# print(myvar)


# def count_cars(cars):
#     print("the number of cars is:"+str(len(cars)))
#
# count_cars("Audi","mersedes","toyota")

# def my_function(*kids):
#     print("The youngest child is "+kids[2])
# my_function("Emil","Tobies","Linus","Mama","Papa")

# def myfunc(n):
#     return lambda a : a * n
# mydoubler = myfunc(2)
# mytriple = myfunc(3)
#
# print(mydoubler(11))
# print(mytriple(11))

# a = ['cписок']
#
# b = list(("banan","limon",'apelsin'))
# c = list('banan')
#
# # print(a)
# print(b[-2])
# # print(c)

# list = ['apple','banan','cherry','orange','kiwi','melon','mango']
# print(list[3:5])
# list = ['apple','banan','cherry','orange','kiwi','melon','mango']
# print(list[:4])
# list = ['apple','banan','cherry','orange','kiwi','melon','mango']
# print(list[2:])


''' ВЫКЛАДЫ '''
